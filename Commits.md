,m2
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [What are Conventional Commits?](#what-are-conventional-commits)
- [Why Use Conventional Commits?](#why-use-conventional-commits)
  - [Understandability](#understandability)
  - [Automation](#automation)
- [How to Use Conventional Commits](#how-to-use-conventional-commits)
  - [Types of Commits](#types-of-commits)
    - [Breaking Change](#breaking-change)
    - [Revert](#revert)
    - [Merge](#merge)
  - [Scopes for Commits](#scopes-for-commits)
  - [Commit Messages](#commit-messages)
    - [Subject Message](#subject-message)
    - [Body Message](#body-message)
    - [Footer](#footer)
  - [Examples](#examples)
    - [Commit message with scope](#commit-message-with-scope)
    - [New feature with breaking change in the Body](#new-feature-with-breaking-change-in-the-body)
    - [Revert of previous commit with body and footer](#revert-of-previous-commit-with-body-and-footer)
- [External Sources](#external-sources)

<!-- /code_chunk_output -->

# What are Conventional Commits?

A convention for commit messages which provides an easy set of rules for creating an explicit commit history.  
This makes it easier to write automated tools -- such as a [Changelog generator](https://medium.com/jobtome-engineering/how-to-generate-changelog-using-conventional-commits-10be40f5826c) -- on top of.  

# Why Use Conventional Commits?

## Understandability

Commits are more descriptive and it's easier to understand the project's history.  
This makes it easier to contribute to a project using the git history as a reference, especially for new hires.  

## Automation
 
Many tools can be written that take advantage of standard conventional commits. These may include, but would not be limited to:  
- [Changelog generator](https://medium.com/jobtome-engineering/how-to-generate-changelog-using-conventional-commits-10be40f5826c)  
- [Automated versioning and publication of a package](https://itnext.io/how-to-automate-versioning-and-publication-of-an-npm-package-233e8757a526)

# How to Use Conventional Commits

Commits will have a standard structure that serves to describe exactly what happens in that commit, below is the full structure:  

```less
<type>(<scope>): <subject_message>

<optional body_message>

<optional footer(s)>
```

some git GUIs like Azure DevOps will often not display multi-line commit messages unless selected. So it is recommended to use single line commits (except for when working a `revert`, a `BREAKING CHANGE`, or a very large release which requires the additional information). Here is how to write a single line commit from a terminal:  

```sh
git commit -m '<type>(<scope>): <subject_message>'
```

When working a `revert` or a `BREAKING CHANGE`, one should include a body or footer message to clarify things more. Feel free to write commit messages interactively using your preferred editor, or use this non-interactive way to add a subject message and a newline separated body message like so:  

*subject and body*  
```sh
git commit -m '<type>(<scope>): <subject_message>' -m '<body_message>'
```

*subject, body, and footer*
```sh
git commit -m '<type>(<scope>): <subject_message>' -m '<body_message>' -m '<footer>'
```

## Types of Commits

Each type of commit (except for `docs`) should equate to an increment to the code's [semantic versioning](https://semver.org/).  

|   Type    | Semantic Versioning |                                          Definition                                          |
| --------- | ------------------- | -------------------------------------------------------------------------------------------- |
| `<type>!` | MAJOR               | Introduce a breaking change (`<type>` is appended by a `!`). Additional details required.    |
| `feat`    | MINOR               | Add a new feature.                                                                           |
| `fix`     | PATCH               | Fix or patch code.                                                                           |
| `docs`    | (N/A)               | Modify Documentation only.                                                                   |
| `revert`  | (N/A)               | Revert a commit. Additional details required.                                                |
| `merge`   | (N/A)               | Merge a commit, should only be used for merge conflicts. Additional details required. footer |

***NOTE:***  Since `docs` are **'Documentation only changes'**, the code base is not affected and therefore should not equate to an increment to it's semantic versioning. Also for `docs`, scopes will probably not be needed and should be considered optional.  

***NOTE:*** a `revert` should also revert the Semantic Versioning.  

### Breaking Change

When a *breaking change* is introduced, append the `<type>` of commit with a `!`. For example, a new feature which introduces a breaking change will have the following type: `feat!` and this commit will be equivalent to a new *MAJOR* version of the code base. This will be explained more in the examples section.  

Breaking changes should include a description of the new breaking change in the body or footer of the commit message (multi-line commit)  

### Revert

If the commit reverts a previous commit, it should begin with `revert:`, followed by the original header of the reverted commit.  
In the body or footer it should say: `This reverts commit <hash>`, where the hash is the SHA of the commit being reverted.  

### Merge

If the commit fixes a merge conflict, it should begin with `merge:`, followed by the original header of the conflicted commit.  
In the body or footer it should say: `This corrects conflict with commit <hash>`, where the hash is the SHA of the conflicted commit commit.  

## Scopes for Commits

A scope may be provided to a commit’s type, to provide additional contextual information and is contained within parenthesis (e.g. `feat(perf): add ability to parse arrays`).  

Besides the scopes listed below, a team should make new scopes as needed. The key is to be consistent and agree on which scopes will be used.  

|      scope      |                    Definition                    |
| --------------- | ------------------------------------------------ |
| `conflict`      | Fix a merge conflict                             |
| `accessibility` | Improve accessibility and usage                  |
| `build`         | Modify the build system or external dependencies |
| `cicd`          | Modify CI/CD configuration files and scripts     |
| `perf`          | Improve performance                              |
| `refactor`      | Improve code                                     |
| `resolve`       | Resolve issue in code review                     |
| `style`         | Modify code style (white-space, formatting, etc) |
| `test`          | Add tests or correct existing tests              |

## Commit Messages

### Subject Message

The first line of a commit message. A short descriptive summary using the imperative present tense where the first letter is not capitalized.  
*Most tools only show the first line of the commit message, which is usually enough to know what it is about. It’s like the subject line in an email*  
- Written with as few words as possible (50 character soft limit)  
    - The first letter is lowercase  
    - Imperative present tense  
        - Use present tense (*"change"* **not** *“changed"* nor *“changes"*) for a commit to serve as instructions for what applying the commit will do to the [trunk](./Trunk-Based-Development.md)  
        - Imperative sentences are used to convey a command. This further serves as an instruction set for what applying the commit will do to the [trunk](./Trunk-Based-Development.md)  

### Body Message

***NOTE:*** Optional **unless** performing a *Breaking Change* or a *Revert*  
- Body Messages should always come after a *subject message* and a blank line  
- Wrap each line to 72 characters, otherwise reading the commit might become an issue  
- Just as in the subject, use the imperative present tense: (*"change"* **not** *“changed"* nor *“changes"*).  
- Include motivation for the change, and contrast it's implementation with previous behavior.  
- Github and Azure DevOps support [Markdown syntax](https://github.github.com/gfm/), so one could write a body message with bullets (`-` or `*`) and checkboxes (`- [ ] foo` or `- [x] bar`)  
- Breaking Change and revert details may be moved to the footer of the commit (especially if the body message is large)  

### Footer

Optional message that might come after the body while performing a *Breaking Change* or a *Revert*  
- Footers should always come after a *Body message* and a blank line  
- Wrap each line to 72 characters, otherwise reading the commit might become an issue  

## Examples

### Commit message with scope

``` less
feat(perf): allow provided config object to extend other configs
```

### New feature with breaking change in the Body

```less
feat!(refactor): drop support for Node 6

BREAKING CHANGE: refactor to use JavaScript features not available in Node 6.
```

### Revert of previous commit with body and footer

```less
revert: 'feat!(refactor): drop support for Node 6'

Will break auth due to dependencies.  
This revert will make the release stable again.

This reverts commit 06faab4d
```

# External Sources

[A Note About Git Commit Messages](https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)  
[Commit Verbs 101](https://medium.com/@danielfeelfine/commit-verbs-101-why-i-like-to-use-this-and-why-you-should-also-like-it-d3ed2689ef70)
[conventionalcommits.org](https://www.conventionalcommits.org/en/v1.0.0/)  
[Enhance your git log with conventional commits](https://dev.to/maxpou/enhance-your-git-log-with-conventional-commits-3ea4)  
[Markdown syntax](https://github.github.com/gfm/)  
[The Angular convention](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines)  
[Writing git commit messages](http://365git.tumblr.com/post/3308646748/writing-git-commit-messages)  