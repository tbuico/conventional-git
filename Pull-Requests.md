
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Pull Requests](#pull-requests)
  - [Title](#title)
  - [Description](#description)
  - [Reviewers](#reviewers)
  - [Work Items](#work-items)
  - [Tags](#tags)
- [Examples](#examples)
- [External Sources](#external-sources)

<!-- /code_chunk_output -->

# Pull Requests

Pull Requests consists of several parts:  
1. **Title**: subject line of a feature/fix [commit message](./Commits.md)  
2. **Description**: Optional detailed explanation of changes which will happen to the [trunk](./Trunk-Based-Development.md) branch once this branch is merged  
3. **Reviewers**: List of required and optional reviewers (imports from branch policy)  
4. **Work Items**: Ticket that captures the work being merged  
5. **Tags**: Optional tag of the new release for this merge  

## Title

The title should be the subject line of a feature/fix commit message that encompasses the changes which will happen to the [trunk](./Trunk-Based-Development.md) branch once this branch is merged.  
By default, the last commit message made to the branch will be used as the title. Ensure that the title properly illustrates all changes that will impact the trunk when merged and has the correct `type` and `scope` (as documented in [Conventional Git: Commits](./Commits.md)), update as needed.  
***NOTE:*** For documentation on how to write standardized commit messages, see [Conventional Git: Commits](./Commits.md)  

## Description

This is an optional field which equates to a commit message's body. Refer to the documentation in [Conventional Git: Commits](./Commits.md) on how to write a standardized **Body Message**.  

## Reviewers

List of people that can review the pull request.  
Ideally, the Required reviewers have been preconfigured for the [trunk](./Trunk-Based-Development.md) branch inside of the `trunks`'s branch policies.  
However both required and optional reviewers can be added to the pull request here.  

## Work Items

Any work items (TIX, BUG, etc. tickets/stories) that relate to the work in the pull request.  
Best practice would be to only have one work item per each pull release. As all pull requests should be as small of a set of work as possible.  

## Tags

This is an optional field.  
Git has the ability to tag specific points in a repository’s history as being important. Typically, people use this functionality to mark release points.  

# Examples

For examples on how to write a Pull request title and description, please see [Conventional Git: Commits](./Commits.md).  

# External Sources

[conventionalcommits.org](https://www.conventionalcommits.org/en/v1.0.0/)  
[Git Basics - Tagging](https://git-scm.com/book/en/v2/Git-Basics-Tagging)  
[pull request naming](https://namingconvention.org/git/pull-request-naming.html)  
[trunkbaseddevelopment.com](https://trunkbaseddevelopment.com/)  