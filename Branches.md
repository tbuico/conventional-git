
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Creating Branches](#creating-branches)
- [Branch Names](#branch-names)
  - [Recommended Naming Conventions](#recommended-naming-conventions)
  - [Optional Naming Conventions](#optional-naming-conventions)
  - [Examples](#examples)
    - [Examples of Recommended Naming Conventions](#examples-of-recommended-naming-conventions)
    - [Examples of Optional Naming Conventions](#examples-of-optional-naming-conventions)
      - [Prepend branch name with Types](#prepend-branch-name-with-types)
      - [Branch name with Type and Scope](#branch-name-with-type-and-scope)
- [External Sources](#external-sources)

<!-- /code_chunk_output -->

# Creating Branches

The creation of new branches should conform to [Conventional Git: Trunk Based Development](./Trunk-Based-Development.md).  

As a result, any new branches that are created in a repo should be considered a short-lived feature or fix which will be deleted once merged into the `trunk` branch.  

# Branch Names

## Recommended Naming Conventions

1. A short descriptive summary in all lowercase using the imperative present tense  
    - Written with as few words as possible (2 or 3 words ideally)  
    - All lowercase  
    - Imperative present tense  
        - Use present tense (*"change"* **not** *"changed"* nor *"changes"*) for a branch name to serve as instructions for what merging the branch will do to the [trunk](./Trunk-Based-Development.md)  
        - Imperative sentences are used to convey a command. This further serves as an instruction set for what merging the branch will do to the [trunk](./Trunk-Based-Development.md)  
2. Use slashes `/` to separate structural parts of the branch name  
    - Slashes are the most flexible delimiter for branches.  
        - Slashes are shell tab expansion (command completion) friendly  
        - Slashes let you do branch renaming when pushing or fetching to/from a remote  
            ```sh
            $ git push origin 'refs/heads/feature/*:refs/heads/feat/*'
            $ git push origin 'refs/heads/bug/*:refs/heads/fix/*'
            ```
    - Because branches are implemented as paths when using slashes, you cannot have a branch named `foo` and another branch named `foo/bar`  
3. No bare numbers. Numbers should be prefixed by a `#` or a string prefix 
    - git may decide that a bare number is part of a sha instead of a branch name  
    - Examples of how to write numbers correctly:  
        ```less
        #297
        ```
        or  
        ```less
        TIX-297
        ```
4. Use hyphens `-` instead of spaces for separating words  
5. No spaces or other special characters besides `/`,`-`,`#`. As this will cause git to error  
    - Cannot contain a space ` `, tilde `~`, caret `^`, colon `:`, question-mark `?`, asterisk `*`, open bracket `[`, backslash `\`, an at symbol `@`, consecutive dots `..`, or consecutive slashes `//` anywhere  
    - Cannot begin or end with a slash `/` or dash `-`  
    - No slash-separated component can begin with a dot `.` or end with the sequence `.lock`  
    - Cannot have ASCII control characters (i.e. bytes whose values are lower than `\040`, or `\177 DEL`) anywhere  
    - Cannot contain a sequence `@{`  
6. Work item appended to end of branch name  

## Optional Naming Conventions

- Use work type as a group token  
    Just as specified in [Conventional Git: Commits](./Commits.md), each type (except for `docs`) should equate to an increment to the code's [semantic versioning](https://semver.org/).  

- Include a scope for intended work with type group tokens  
    Each of these scopes are intended to explain which portion of work each branch belongs to.  
    Just as specified in [Conventional Git: Commits](./Commits.md).

## Examples

### Examples of Recommended Naming Conventions

*Summary and work-item only*  

* Structure
    ```less
    <summary>/<work-item-id>
    ```

* Example
    ```sh
    git checkout -b enable-sso/TIX-244
    ```

### Examples of Optional Naming Conventions

#### Prepend branch name with Types

* Structure
    ```less
    <type>/<summary>/<work-item-id>
    ```

* Example
    ```sh
    git checkout -b feat/enable-sso/TIX-244
    ```

#### Branch name with Type and Scope

* Structure
    ```less
    <type>/<scope>/<summary>/<work-item-id>
    ```

* Example
    ```sh
    git checkout -b feat/auth/enable-sso/TIX-244
    ```

# External Sources

[branch-naming](https://namingconvention.org/git/branch-naming.html)  
[commonly used practices for naming git branches](https://stackoverflow.com/questions/273695/what-are-some-examples-of-commonly-used-practices-for-naming-git-branches#6065944)  
[conventionalcommits.org](https://www.conventionalcommits.org/en/v1.0.0/)  
[git-check-ref-format(1) Manual Page](https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-check-ref-format.html)  
[semantic versioning](https://semver.org/)  
[Writing git commit messages](http://365git.tumblr.com/post/3308646748/writing-git-commit-messages)  