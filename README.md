
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Intention of these docs](#intention-of-these-docs)
- [Influences](#influences)
- [Documentation](#documentation)

<!-- /code_chunk_output -->

# Intention of these docs
This documentation is to serve as a generic guide to git standardization.  

And has been since called **Conventional Git flavored Trunk Based Development**

# Influences
It has largely been influenced by the following standard resources:  
- [trunkbaseddevelopment.com](https://trunkbaseddevelopment.com/)  
- [conventionalcommits.org](https://www.conventionalcommits.org/en/v1.0.0/)  
- [semver.org](https://semver.org/)

# Documentation

*In order of importance*

|                               Doc                               |                                                Purpose                                                |
| --------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| [Trunk Based Development](./Trunk-Based-Development.md) | Explains What Trunk Based Development is, why it should be used, and how to use it (with examples)    |
| [Commits](./Commits.md)                                         | Explains What Conventional commits are, why they should be used, and how to use them (with examples)  |
| [Branches](./Branches.md)                                       | Outlines the recommended way to name Branches for better understanding and a cleaner git history      |
| [Pull Requests](./Pull-Requests.md)                         | Outlines the recommended way to name Pull Requests for better understanding and a cleaner git history |