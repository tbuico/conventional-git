
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [What is Trunk Based Development?](#what-is-trunk-based-development)
- [Why use Trunk Based Development?](#why-use-trunk-based-development)
  - [short-lived feature/fix branches](#short-lived-featurefix-branches)
  - [Trunk-Based Development will always be release ready](#trunk-based-development-will-always-be-release-ready)
- [How to Use Trunk Based Development](#how-to-use-trunk-based-development)
  - [Checking out / cloning](#checking-out-cloning)
  - [Commits](#commits)
  - [Code Reviews](#code-reviews)
  - [Releasing](#releasing)
  - [Continuous Integration (CI)](#continuous-integration-ci)
  - [Team Consideration](#team-consideration)
- [Convert a Repo that Uses Long-Living Branches to Use Trunk Based Development](#convert-a-repo-that-uses-long-living-branches-to-use-trunk-based-development)
  - [Cleanup](#cleanup)
  - [Set up TBD](#set-up-tbd)
- [External Sources](#external-sources)

<!-- /code_chunk_output -->

# What is Trunk Based Development?

1. There is one `trunk` branch per repo; this is often called the `master` branch, but may be named anything.  
2. There is no `develop` branch or other long-living branches.  
    - long-living branches increase conflicts, confusion, and errors.
3. All development work is done inside short-lived feature/fix branches.  
    - this helps reduce conflicts, and increases deployment cadence.
4. The `trunk` branch is always release ready and will never cause issues if released to production.   

# Why use Trunk Based Development?

## short-lived feature/fix branches

Advantages to using short-lived feature/fix branches:
- Smaller pull/merge requests  
- Reduction of merge conflicts  
- Reduction of bugs  
- Work can be planned more accurately  
- Enables regular release cadence  
- Enables modular design (see [The Unix Philosophy](https://en.m.wikipedia.org/wiki/Unix_philosophy))  

short-lived feature/fix branches are used to either add new features or apply fixes/patches. Once code on the new branch compiles, passes all tests, and goes through a code review process, the code is merged straight to `trunk`.  
*preferably with commit history of the feature branch being squashed on merge to keep the git history/log clean.*  
After the short lived branch is merged, it should be deleted from both the origin and remote.

By merging to the `trunk` often, merge conflicts become very rare. Even if a merge conflict emerges, it should be easier to solve because the pull/merge requests should be smaller and contain less differences from the `trunk`. Interferences between different features become visible immediately and can be tested while the features are still in progress.  

Smaller features are easier to plan in higher detail, with more concise acceptance criteria and a better understanding of how a story/task should be pointed. This can also help the release cadence become more regular, which enables the ability to manage and plan for an upcoming release more effectively and with less guess work.  

When planning to deploy smaller features it is more obvious how those features will need to work with each other in the new release as a whole. This may enable a more modular design to the release which conforms to something called [The Unix Philosophy](https://en.m.wikipedia.org/wiki/Unix_philosophy) which favors decomposability over monolithic design.  

## Trunk-Based Development will always be release ready

Short lived feature/fix branches should only be pulled into the `trunk` branch if they are release ready. Pull/merge requests should never break the build.  

# How to Use Trunk Based Development

*This document will specify Conventional Git flavored Trunk Based Development*

## Checking out / cloning

All developers in a team working on an application/service will clone, checkout, and create new short-lived feature/fix branches from the `trunk`.  
They may update/pull/sync from the `trunk` many times a day, knowing that the build passes.  

## Commits

Follow the documentation found in [Conventional Git - Commits](./Commits.md)  

## Code Reviews

Commits to be reviewed should be as small as possible, and ideally encompass the work from one story/task. When a change is too large, the person reviewing the code might ignore or miss important things. Whereas if the change is small, it’s easier to make useful recommendations.  
Developers completing a piece of development work (changes to source code), that does not break the build, will commit it to a short-lived feature/fix branch used for code-review and build checking (CI).  
Teams will follow a conventional design where the commit is marshaled for review using a pull/merge request (PR) that is visible to the team before landing in the `trunk`.  

![code-review](assets/trunk-based-development_code-review.png)  
> the speech bubbles are stylized code review comments  

- Code review branches can (and should) be deleted after the code review is complete and be very short-lived.  
- Squash merges should be the preferred (default) merge type, other merge types should only be considered when necessary.  
- You may want to keep the commentary/approval/rejection that is part of the review for historical and auditing purposes, but you do not want to keep the branch. Specifically, you do not want the developers to focus on the branch after the code review and merge back to the `trunk`.  

Simultaneous work on 2 (or more) short-lived feature branches with the end goal of merging to `trunk` after passing quality gates is something that will happen often. The key is simply to merge **from** `trunk` before attempting to merge **to** trunk with a PR.

![slfb_pull-push](assets/trunk-based-development_slfb_pull-push.png)

Below is how `trunk` will look like after the commit passed code review and was squash merged and then tagged for release.  

![release_from_trunk](assets/trunk-based-development_release_from_trunk.png)  

## Releasing

Teams that use Continuous Delivery or have a high release cadence should release directly [from the trunk](https://trunkbaseddevelopment.com/release-from-trunk/)  
- Fixes/patches when Releasing from `trunk`  
    - Teams should make a branch, to cherry-pick the fix/patch to and release from before pushing to the `trunk`.  
      **NOTE:** Branches can be made retroactively  
    - Teams may also choose to roll forward and apply a fix/patch on the `trunk` as if it were a feature, albeit as quickly as possible.  

Teams that **do not** use Continuous Delivery or have a lower release cadence might make [branches for release](https://trunkbaseddevelopment.com/branch-for-release/) from the `trunk`.  

**NOTE:** Our goal is to have CI/CD and move to a higher release cadence. So we should *release directly from the trunk* not use *branches for release*.  

## Continuous Integration (CI)

Pipelines are setup to watch the `trunk` with the intention of failing as quickly and noticeably as possible to grab the team's attention.  
The pipeline could revert the pull/merge request to the previous commit automatically after a build fails, or teams can then manually revert to the previous commit.  
For short-lived feature/fix branches, pipelines can either be made to watch that branch too or be manually ran against the feature branch to enable the review process.  

## Team Consideration

Besides not breaking the build in the `trunk` branch, the team should also consider the impact of their potentially larger commits. Especially where renames or moves were wholesale, and adopt techniques to allow those changes to be more easily consumed by teammates.  

# Convert a Repo that Uses Long-Living Branches to Use Trunk Based Development

*Assuming that the `trunk` branch will be called `master`*

## Cleanup

1. Commit current work (as every change should be regularly committed).  
2. Remove any stale or orphaned feature/fix branches, use your own judgement (this should be done regularly).  
3. If there are existing pull/merge requests, review them.  
    - If they pass review, merge them into the long-living branch (the branch which is planned to be merged into the `trunk` branch).  
    - If they **do not** pass review, abandon the pull/merge requests and create a new pull/merge request with the destination branch as the `trunk` branch.  

![cleanup](assets/trunk-based-development_cleanup.png)  

## Set up TBD

4. Leave all feature/fix branches alone from this point on until the rest of the process is finished.  
5. Ensure that there are no build validations or triggers associated with any long living branches.  
6. Ensure that `master` is set as both the `default` and the `compare` branch.  
7. Create a pull/merge requests to merge the long-living branches into the `trunk` branch, if the review passes then merge.  
8. Lock the long-living branch.  
9. Delete the long-living branch.  
10. From now on all feature/fix branches will be merged into master via a pull/merge request as specified in the [How to Use Trunk Based Development](#how-to-use-trunk-based-development) section.  

# External Sources

[conventionalcommits.org](https://www.conventionalcommits.org/en/v1.0.0/)  
[trunk based development: what & why](https://rollout.io/blog/trunk-based-development-what-why/)  
[trunkbaseddevelopment.com](https://trunkbaseddevelopment.com/)  